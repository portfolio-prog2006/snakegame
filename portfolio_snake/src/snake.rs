use std::collections::LinkedList;
use piston_window::{Context,G2d};
use piston_window::Key::B;
use piston_window::types::Color;

use draw::draw_block;
use crate::draw;

// Colour of the snake as rgba
const SNAKE_COLOUR: Color = [0.2, 0.2, 0.8, 0.8];

// Enum for direction of the snake, used when handling key-events
#[derive(Copy, Clone, PartialEq)]
pub enum Direction {
    Up,
    Down,
    Left,
    Right,
}

// Function for determining opposite direction to current to handle "illegal" directions when playing
impl Direction {
    pub fn opposite(&self) -> Direction {
        match *self {
            Direction::Up => Direction::Down,
            Direction::Down => Direction::Up,
            Direction::Left => Direction::Right,
            Direction::Right => Direction::Left,
        }
    }
}

// Block structs (snake food)
#[derive(Debug, Clone)]
struct Block{
    x: i32,
    y: i32,
}

// Snake struct
pub struct Snake {
    direction: Direction,   // Direction of movement
    body: LinkedList<Block>,    // List of blocks, new added each time it eats a block
    tail: Option<Block>,    // Since tail isn't deleted when the snake moves unless game over
}

// Functions for the snake struct (such as new snake, draw snake, find head position, and move)
impl Snake {
    // Owned function to create a new snake at starting position going right with a body of 3 blocks
    pub fn new(x: i32, y:i32) -> Snake {
        let mut body: LinkedList<Block> = LinkedList::new();
        body.push_back(Block{
           x: x + 2,
            y,
        });
        body.push_back(Block{
            x: x + 1,
            y,
        });
        body.push_back(Block{
            x,
            y,
        });

        Snake {
            direction: Direction::Right,
            body,
            tail: None,
        }
    }

    // Draws the blocks of the body to the window
    pub fn draw(&self, context: &Context, g: &mut G2d) {
        for block in &self.body {
            draw_block(SNAKE_COLOUR,block.x, block.y, context, g);
        }
    }

    // Returns the x and y coords of the first block of the snake (the "head" block)
    pub fn head_position(&self) -> (i32, i32){
        let head_block = self.body.front().unwrap();
        (head_block.x, head_block.y)
    }

    // Function for moving the snake forward
    pub fn move_forward(&mut self, dir: Option<Direction>) {
        match dir{
            Some(d) => self.direction = d,
            None => (),
        }

        let (last_x, last_y): (i32,i32) = self.head_position();

        // origo for the window is upper left corner (like a two-dim array)
        let new_block = match self.direction {
            Direction::Up => Block {
                x: last_x,
                y: last_y - 1,
            },
            Direction::Down => Block {
                x: last_x,
                y: last_y + 1,
            },
            Direction::Left => Block {
                x: last_x - 1,
                y: last_y,
            },
            Direction::Right => Block {
                x: last_x + 1,
                y: last_y,
            },
        };
        // Adding a block to the front of the snake to simulate movement in the desired direction
        self.body.push_front(new_block);
        let removed_block = self.body.pop_back().unwrap();
        self.tail = Some(removed_block);
    }

    // Retrieves direction of the head of the snake
    pub fn head_direction(&self) -> Direction {
        self.direction
    }

    // Calculates the next position of the first block of the snake based on the direction it's going
    pub fn next_head(&self, dir: Option<Direction>) -> (i32, i32){
        let (head_x, head_y): (i32,i32) = self.head_position();

        let mut moving_dir = self.direction;
        match dir {
            Some(d) => moving_dir = d,
            None => {}
        }

        match moving_dir {
            Direction::Up => (head_x,head_y - 1),
            Direction::Down => (head_x,head_y + 1),
            Direction::Left => (head_x - 1,head_y),
            Direction::Right => (head_x + 1,head_y),
        }
    }

    // Pushes the cloned tail to the back of the body (when a block has been eaten). Makes the snake grow
    pub fn restore_tail(&mut self) {
        let blk = self.tail.clone().unwrap();
        self.body.push_back(blk);
    }

    // Checks for overlap (the snake crashes into itself)
    pub fn overlap_tail(&self, x: i32, y: i32) -> bool {
        let mut ch = 0;
        for block in &self.body {
            if x == block.x && y == block.y {
                return true;
            }

            // Allows the head and tail to momentarily occupy the same block
            ch += 1;
            if ch == self.body.len() - 1 {
                break;
            }
        }
        return false;
    }
}