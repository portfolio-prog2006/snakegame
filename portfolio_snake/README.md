

### *DISCLAIMER: this is not my original code but rather a demonstration of Rust for a school portfolio. I followed a tutorial to see how Rust works for games-programming and all credit goes to **Tensor Programming** on YouTube:* [Tensor Programming's Snake Game in Rust](https://youtu.be/DnT_7M7L7vo)

----------------

# snakeGame

## Description
This is a demonstration of how Rust can be used for game programming, and an implementation of the classic snake game.

## Contents
*three .rs files for impl functions and structs, as well as consts*
- `draw.rs`
- `game.rs`
- `snake.rs`

all put together in `main.rs`

#### Crates used
- rand = "0.8.5"
- piston_window = "0.131.0"


## Visuals
#### *Startup Screen:*
![Startup screen with snake and food](snake1.PNG "Snake Game")

#### *Death Screen:*
![Snake has crashed in the wall, screen goes red](resetScreen.PNG "On death")

## Installation
```
clone the project to your repo

Using cargo for Rust:

cargo build // You can skip this step
cargo run
```

